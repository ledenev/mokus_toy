# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .generic_model import *


class Scooter(GenericProduct):
    class Meta:
        verbose_name = "Самокат"
        verbose_name_plural = "Самокаты"

    max_weight = models.CharField(max_length=32,null=True,blank=True,verbose_name='Макс. вес катающегося')
    wheel_count = models.CharField(max_length=32,null=True,blank=True,verbose_name='Количество колес')
    platform_material=models.CharField(max_length=32,null=True,blank=True,verbose_name='Материал платформы')
    is_seat = models.BooleanField(default=False, verbose_name='Сидение')
    is_diod = models.BooleanField(default=False, verbose_name='Светодиодные колеса')





class Greenhouse(GenericProduct):
    class Meta:
        verbose_name = "Теплицы и парники"
        verbose_name_plural = "Теплицы и парники"

    greenhouse_type = models.CharField(max_length=32, null=True, blank=True, verbose_name='Тип')
    shape_type = models.CharField(max_length=32, null=True, blank=True, verbose_name='Форма')
    frame_type = models.CharField(max_length=64, null=True, blank=True, verbose_name='Каркас')
    covering_type = models.CharField(max_length=64, null=True, blank=True, verbose_name='Покрытие')
    snowload = models.CharField(max_length=32, null=True, blank=True, verbose_name='Снеговая нагрузка')
    doors_exist = models.BooleanField(default=False, verbose_name='Наличие дверей')
    doors = models.CharField(max_length=32, null=True, blank=True, verbose_name='Двери')
    windows_exist = models.BooleanField(default=False, verbose_name='Наличие форточек')
    windows = models.CharField(max_length=32, null=True, blank=True, verbose_name='Форточки')
    hatchs_exist = models.BooleanField(default=False, verbose_name='Наличие подъемных частей крыши (люков)')
    hatchs = models.CharField(max_length=32, null=True, blank=True, verbose_name='Подъемные части крыши (люки)')
    pipe_section = models.CharField(max_length=32, null=True, blank=True, verbose_name='Сечение профиля (трубы)')
    pipe_gage = models.CharField(max_length=32, null=True, blank=True, verbose_name='Толщина профиля (трубы)')
    covering_mount_type = models.CharField(max_length=32, null=True, blank=True,
                                           verbose_name='Крепление поликарбоната')
    lug_exist = models.BooleanField(default=False, verbose_name='Наличие грунтозацепов')
    lug = models.CharField(max_length=32, null=True, blank=True, verbose_name='Грунтозацепы')
    framework_impr_exist = models.BooleanField(default=False, verbose_name='Наличие усилители дуг')
    framework_impr = models.CharField(max_length=32, null=True, blank=True, verbose_name='Усилители дуг')
    length = models.CharField(max_length=32, null=True, blank=True, verbose_name='Длина')
    middle_section_length = models.CharField(max_length=32, null=True, blank=True,
                                             verbose_name='Длина торцевых секций')
    front_section_length = models.CharField(max_length=32, null=True, blank=True, verbose_name='Длина вставки')
    pipe_step = models.CharField(max_length=32, null=True, blank=True, verbose_name='Расстояние (шаг) между дугами')
    width = models.CharField(max_length=32, null=True, blank=True, verbose_name='Ширина')
    height = models.CharField(max_length=32, null=True, blank=True, verbose_name='Высота')
    weight = models.CharField(max_length=32, null=True, blank=True, verbose_name='Вес')


