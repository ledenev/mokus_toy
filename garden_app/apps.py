# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig


class GardenAppConfig(AppConfig):
    name = "garden_app"
    verbose_name = "Магазин"

# TODO: P2 Add filtration by categories and search
# TODO: P3 Rename garden_app to shop_app
