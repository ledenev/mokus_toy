# -*- coding: utf-8 -*-
from django.contrib import admin
from .forms import *
from .models import *
from django.forms import Textarea
from django.db import models
import copy  # (1) use python copy


class PropertyImageInline(admin.TabularInline):
    model = GenericProductImage
    readonly_fields = ["admin_image"]
    fields = ["admin_image", "image"]
    extra = 0


def publish_product(modeladmin, request, queryset):
    queryset.update(is_published=True)

publish_product.short_description = "Опубликовать выбранное"


def hide_product(modeladmin, request, queryset):
    queryset.update(is_published=False)

hide_product.short_description = "Спрятать выбранное"





class ProductAdminGeneric(admin.ModelAdmin):
    save_on_top = True
    save_as = True
    inlines = [PropertyImageInline, ]
    form = GenericProduct
    prepopulated_fields = {'slug': ('name', )}
    list_display = ["name", "is_published", "price", 'show_order', "updated_date"]
    list_editable = ('price', 'show_order', )
    list_filter = ["is_published", "created_date", "updated_date", "price", "is_discount", "producer"]
    search_fields = "name", "description"

    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 60})},
    }
    ordering = ('name',)
    actions = [publish_product, hide_product]

    def save_model(self, request, obj, form, change):
        if obj.created_by == '':
            obj.created_by = request.user.username
        obj.updated_by = request.user.username
        obj.save()


class GreenhouseAdmin(ProductAdminGeneric):
    form = GreenhouseAdminForm
    fieldsets = (
        ('Основные парамерты', {
            'fields': ('is_published', 'show_order', 'name', 'slug', 'short_desc',
                       'description', 'price', 'is_discount', 'prev_price', 'producer')
        }),
        ('SEO парамерты', {
            'classes': ('collapse',),
            'fields': ('seo_title', 'seo_description', 'seo_keywords')
        }),
        ('Характеристики', {
            'fields': (
            'greenhouse_type', 'shape_type', 'frame_type', 'covering_type', 'snowload', 'doors_exist', 'doors',
            'windows_exist', 'windows', 'hatchs_exist', 'hatchs', 'pipe_section', 'pipe_gage', 'covering_mount_type',
            'lug_exist','lug', 'framework_impr_exist', 'framework_impr', 'length', 'middle_section_length',
            'front_section_length', 'pipe_step', 'width', 'height', 'weight',),
        }),
    )


class ScooterAdmin(ProductAdminGeneric):
    form = ScooterAdminForm
    fieldsets = (
        ('Основные парамерты', {
            'fields': ('is_published', 'product_type', 'name', 'slug', 'short_desc',
                       'description', 'price', 'is_discount', 'prev_price', 'producer')
        }),
        ('SEO парамерты', {
            'classes': ('collapse',),
            'fields': ('seo_title', 'seo_description', 'seo_keywords')
        }),
        ('Характеристики', {
            'fields': (
            'max_weight', 'wheel_count', 'platform_material', 'is_seat', 'is_diod',),
        }),
    )

admin.site.register(Greenhouse, GreenhouseAdmin)
admin.site.register(Scooter, ScooterAdmin)
