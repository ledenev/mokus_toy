from django import forms
from redactor.widgets import RedactorEditor
from .models import *


class GreenhouseAdminForm(forms.ModelForm):
    class Meta:
        model = Greenhouse
        fields = '__all__'
        widgets = {
           'description': RedactorEditor(),
        }


class ScooterAdminForm(forms.ModelForm):
    class Meta:
        model = Scooter
        fields = '__all__'
        widgets = {
           'description': RedactorEditor(),
        }


