from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^list/$', ListView.as_view(), name='list'),
    url(r'^list/(?P<model>[\w-]+)/$', ListProductView.as_view(), name='product_list'),
    url(ur'^list/(?P<model>[\w-]+)/(?P<filter_name>[\w-]+)/(?P<filter_value>[\w\s.,-/%]+)/$',
        ListProductFilterView.as_view(), name='product_list_filter'),
    url(r'^(?P<slug>[\w-]+)/$', ProductView.as_view(), name='product'),
]
