# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-24 19:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('garden_app', '0009_auto_20160124_2023'),
    ]

    operations = [
        migrations.DeleteModel(
            name='StaticPage',
        ),
    ]
