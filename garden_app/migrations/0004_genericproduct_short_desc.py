# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-23 23:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('garden_app', '0003_auto_20160118_0140'),
    ]

    operations = [
        migrations.AddField(
            model_name='genericproduct',
            name='short_desc',
            field=models.CharField(default='null', max_length=1000),
        ),
    ]
