# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-13 12:24
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('garden_app', '0018_auto_20160312_2322'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='polycarbonate',
            options={'verbose_name': '\u041f\u043e\u043b\u0438\u043a\u0430\u0440\u0431\u043e\u043d\u0430\u0442 \u0434\u043b\u044f \u0442\u0435\u043f\u043b\u0438\u0446', 'verbose_name_plural': '\u041f\u043e\u043b\u0438\u043a\u0430\u0440\u0431\u043e\u043d\u0430\u0442 \u0434\u043b\u044f \u0442\u0435\u043f\u043b\u0438\u0446'},
        ),
    ]
