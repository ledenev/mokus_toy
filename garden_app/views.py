# -*- coding: utf-8 -*-
from django.apps import apps
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView
from .generic_model import GenericProduct
from django.utils.encoding import *


class IndexView(TemplateView):
    template_name = "index.html"
    context = dict()

    def get(self, request, *args, **kwargs):
        list1 = GenericProduct.objects.filter(is_published=True, product_type__model='greenhouse').order_by('show_order')[0:4]
        list2 = GenericProduct.objects.filter(is_published=True, product_type__model='scooter').order_by('show_order')[0:4]
        self.context['list1'] = list1
        self.context['list2'] = list2
        return render(request, self.template_name, self.context)


class ListView(TemplateView):
    template_name = "product_list.html"
    context = dict()

    def get(self, request, *args, **kwargs):
        product_list = GenericProduct.objects.filter(is_published=True).order_by('-created_date')

        paginator = Paginator(product_list, 12)
        page = request.GET.get('page')

        try:
            products = paginator.page(page)
        except PageNotAnInteger:  # If page is not an integer, deliver first page.
            products = paginator.page(1)
        except EmptyPage:  # If page is out of range (e.g. 9999), deliver last page of results.
            products = paginator.page(paginator.num_pages)

        self.context['products'] = products
        return render(request, self.template_name, self.context)


class ListProductView(TemplateView):
    template_name = "product_list.html"
    context = dict()

    def get(self, request, *args, **kwargs):
        product_list = GenericProduct.objects.filter(product_type__model=kwargs['model'], is_published=True).order_by('-created_date')

        paginator = Paginator(product_list, 12)
        page = request.GET.get('page')

        try:
            products = paginator.page(page)
        except PageNotAnInteger:  # If page is not an integer, deliver first page.
            products = paginator.page(1)
        except EmptyPage:  # If page is out of range (e.g. 9999), deliver last page of results.
            products = paginator.page(paginator.num_pages)

        self.context['model'] = kwargs['model']
        self.context['products'] = products
        return render(request, self.template_name, self.context)


class ListProductFilterView(TemplateView):
    template_name = "product_list.html"
    context = dict()

    def get(self, request, *args, **kwargs):

        products_class = apps.get_model(app_label='garden_app', model_name=kwargs['model'])
        filter_dict = {'is_published': True,
                       kwargs['filter_name']: uri_to_iri(kwargs['filter_value']),  # uri_to_iri - Костыль для hoster.by
                       }

        product_list = products_class.objects.filter(**filter_dict).order_by('-created_date')

        paginator = Paginator(product_list, 12)
        page = request.GET.get('page')

        try:
            products = paginator.page(page)
        except PageNotAnInteger:  # If page is not an integer, deliver first page.
            products = paginator.page(1)
        except EmptyPage:  # If page is out of range (e.g. 9999), deliver last page of results.
            products = paginator.page(paginator.num_pages)

        self.context['filter_value'] = uri_to_iri(kwargs['filter_value'])   # uri_to_iri - Костыль для hoster.by
        self.context['model'] = kwargs['model']
        self.context['products'] = products
        return render(request, self.template_name, self.context)


class ProductView(TemplateView):
    template_name = "product_%s.html"
    context = dict()

    def get(self, request, *args, **kwargs):
        product = get_object_or_404(GenericProduct, slug=kwargs['slug'], is_published=True)
        self.template_name %= product.product_type.model
        self.context['product'] = product.child_object
        return render(request, self.template_name, self.context)
