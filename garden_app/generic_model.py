# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import math
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.html import format_html
from django.contrib.contenttypes.models import ContentType

class GenericProduct(models.Model):
    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    id = models.AutoField(primary_key=True)
    product_type = models.ForeignKey(ContentType, blank=True, null=True, verbose_name='Тип объекта')
    is_published = models.BooleanField(verbose_name="Опубликовано")
    show_order = models.FloatField(verbose_name="Сортировка", default=0, blank=True, null=True)
    name = models.CharField(max_length=250, verbose_name="Название")
    slug = models.SlugField(max_length=250, unique=True, verbose_name="Web ссылка")
    created_date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Дата создания")
    created_by = models.CharField(max_length=32, blank=True, verbose_name="Кем создано")
    updated_date = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name="Дата обновления")
    updated_by = models.CharField(max_length=32, blank=True, verbose_name="Кем обновлено")
    short_desc = models.TextField(max_length=1000, default="null", verbose_name="Краткое описание")
    description = models.TextField(max_length=32000, verbose_name="Полное описание")
    price = models.FloatField(verbose_name="Цена")
    is_discount = models.BooleanField(verbose_name="Скидка")
    prev_price = models.FloatField(null=True, blank=True, verbose_name="Старая цена")
    producer = models.CharField(max_length=250, null=True, blank=True, verbose_name="Производитель")
    seo_title = models.TextField(max_length=255, null=True, blank=True, verbose_name="Заголовок страницы")
    seo_description = models.TextField(max_length=255, null=True, blank=True, verbose_name="Описание страницы")
    seo_keywords = models.TextField(max_length=255, null=True, blank=True, verbose_name="Ключевые слова")

    def child_object(self):
        return self.product_type.get_object_for_this_type(id=self.id)

    def get_absolute_url(self):
        return reverse("product", kwargs={"slug": self.slug})

    def get_order_url(self):
        return reverse("order_product", kwargs={"product": self.slug})

    @property
    def get_discount_size(self):
        return self.prev_price - self.price

    @property
    def get_instalment_payment_3m(self):
        return math.ceil((self.price/0.91)/(3*100))*100

    @property
    def get_instalment_payment_4m(self):
        return math.ceil((self.price/0.89)/(4*100))*100

    @property
    def get_instalment_payment_5m(self):
        return math.ceil((self.price/0.87)/(5*100))*100

    @property
    def get_instalment_payment_6m(self):
        return math.ceil((self.price/0.85)/(6*100))*100

    @property
    def get_instalment_payment_12m(self):
        return math.ceil((self.price/0.74)/(12*100))*100

    def __unicode__(self):
        return self.name

class GenericProductImage(models.Model):
    class Meta:
        verbose_name = "Картинка для товара"
        verbose_name_plural = "Картинки для товара"
    property = models.ForeignKey(GenericProduct, related_name='images')
    image = models.ImageField(verbose_name="Картинка")

    def admin_image(self):
        return format_html('<img src="%s" width="80" />' % self.image.url)

    admin_image.short_description = 'Предпросмотр'

    def admin_size(self):
        nbytes = self.image.size
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
        if nbytes == 0: return '0 B'
        i = 0
        while nbytes >= 1024 and i < len(suffixes) - 1:
            nbytes /= 1024.
            i += 1
        f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
        return 'Image %dx%d (%s %s)' % (self.image.width, self.image.height, f, suffixes[i])

    def __str__(self):
        return self.admin_size()