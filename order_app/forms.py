# -*- coding: utf-8 -*-
from crispy_forms.layout import Submit, Div, Layout, Fieldset
from crispy_forms.helper import FormHelper
from django import forms
from redactor.widgets import RedactorEditor
from .models import Order


class OrderAdminForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'
        widgets = {
           'description': RedactorEditor(),
        }


class OrderForm(forms.Form):
    name = forms.CharField(max_length=250, required=True, label="Имя")
    phone = forms.CharField(min_length=7, max_length=16, required=True, label="Номер телефона")
    email = forms.EmailField(max_length=64, required=False, label="Email адрес (не обязательно)")
    address = forms.CharField(max_length=250, label="Адрес доставки", widget=forms.Textarea(attrs={'rows': 2}), )
    comments = forms.CharField(max_length=250, required=False, label="Дополнительная информация (укажите тип оплаты)",
                               widget=forms.Textarea(attrs={'rows': 2}),)

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-exampleForm'
        #self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset(
                '',
                'name',
                'phone',
                'email',
                'address',
                'comments',
            ),
            Div(
                Div(
                    Submit('submit', u'Заказать', css_class='btn btn-sm btn-success btn-block'),
                    css_class='col-xs-6 col-xs-offset-3',
                ),
                css_class='row',
            )
        )
