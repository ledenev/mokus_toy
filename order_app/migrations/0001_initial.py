# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-08 00:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('garden_app', '0015_auto_20160207_0316'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=250, verbose_name='\u0418\u043c\u044f')),
                ('phone', models.CharField(max_length=16, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430')),
                ('email', models.EmailField(blank=True, max_length=64, null=True, verbose_name='Email \u0430\u0434\u0440\u0435\u0441')),
                ('delivery_address', models.TextField(help_text='\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: \u041c\u0438\u043d\u0441\u043a\u0438\u0439 \u0440\u0430\u0439\u043e\u043d, \u0434\u0435\u0440\u0435\u0432\u043d\u044f \u0413\u043e\u0440\u043e\u0448\u043a\u0438, \u0434\u043e\u043c 17', max_length=250, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438')),
                ('comments', models.TextField(blank=True, max_length=250, null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439')),
            ],
            options={
                'verbose_name': '\u041a\u043b\u0438\u0435\u043d\u0442',
                'verbose_name_plural': '\u041a\u043b\u0438\u0435\u043d\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('status', models.CharField(default=1, max_length=16, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441')),
                ('comments', models.TextField(blank=True, max_length=250, null=True, verbose_name='\u0421\u043b\u0443\u0436\u0435\u0431\u043d\u044b\u0439 \u043a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439')),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('updated_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('updated_by', models.CharField(blank=True, max_length=32, verbose_name='\u041a\u0435\u043c \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e')),
                ('customer', models.OneToOneField(on_delete=models.SET('\u0423\u0434\u0430\u043b\u0435\u043d\u043e'), to='order_app.Customer')),
                ('product', models.OneToOneField(on_delete=models.SET('\u0423\u0434\u0430\u043b\u0435\u043d\u043e'), to='garden_app.GenericProduct')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b',
            },
        ),
    ]
