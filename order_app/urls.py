from django.conf.urls import url
from .views import OrderIndexView, OrderProductView

urlpatterns = [
    #url(r'^order/$', OrderView.as_view(), name='order'),
    url(r'^$', 'order_app.views.OrderProductView', name='order'),
    url(r'^(?P<product>[\w-]+)/$', OrderProductView.as_view(), name='order_product'),
]
