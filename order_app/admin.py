# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.db import models
from django.forms import Textarea
from .forms import OrderAdminForm
from .models import Order
from django.contrib.humanize.templatetags.humanize import intcomma


class OrderPageAdmin(admin.ModelAdmin):
    form = OrderAdminForm
    save_on_top = True

    def get_order_id(self, obj):
        return "%03d" % obj.id
    get_order_id.short_description = '№ Заказа'
    get_order_id.admin_order_field = 'order__id'

    def get_customer_name(self, obj):
        return obj.customer.name
    get_customer_name.short_description = 'Клиент'
    get_customer_name.admin_order_field = 'customer__name'

    def get_customer_phone(self, obj):
        return obj.customer.phone
    get_customer_phone.short_description = 'Телефон'
    get_customer_phone.admin_order_field = 'customer__phone'

    def get_customer_email(self, obj):
        return obj.customer.email
    get_customer_email.short_description = 'Email'
    get_customer_email.admin_order_field = 'customer__email'

    def get_customer_address(self, obj):
        return obj.customer.address
    get_customer_address.short_description = 'Адрес'
    get_customer_address.admin_order_field = 'customer__address'

    def get_product_name(self, obj):
        return obj.product.name
    get_product_name.short_description = 'Продукт'
    get_product_name.admin_order_field = 'product__name'

    def get_product_price(self, obj):
        return intcomma(int(obj.product.price))
    get_product_price.short_description = 'Цена'
    get_product_price.admin_order_field = 'product__price'

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    readonly_fields = ('get_order_id', 'get_customer_name', 'get_customer_phone', 'get_customer_email', 'updated_date',
                       'get_product_name', 'get_product_price', 'get_customer_address', 'comments', 'created_date',
                       'updated_by')
    list_display = ["get_order_id", "status", "created_date", "get_customer_name", "get_customer_phone",
                    "get_product_name", "get_product_price", "staff_comments", "comments"]
    search_fields = ["customer__phone", "customer__name", "staff_comments"]
    list_editable = ['status', 'staff_comments']
    ordering = ['-created_date']
    formfield_overrides = {models.TextField: {'widget': Textarea(attrs={'rows': 1, 'cols': 40})}}
    list_filter = ["status", "created_date"]
    fieldsets = (
        ('Информация от клиента', {
            'fields': ('get_customer_name', 'get_customer_phone', 'get_customer_email', 'get_customer_address',
                       'comments')
        }),
        ('Информация о заказе', {
            'fields': ('created_date', 'updated_date', 'updated_by', 'status', 'staff_comments')
        }),
        ('Информация о товаре', {
            'fields': (
            'get_product_name', 'get_product_price'),
        }),
    )

admin.site.register(Order, OrderPageAdmin)
