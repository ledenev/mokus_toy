# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db import models
from garden_app.models import GenericProduct

STATUS_CHOICES = (
    ('new', 'Новый'),
    ('in_progress', 'В процессе'),
    ('finished', 'Успешно завершён'),
    ('canceled', 'Отменён'),
)


class Customer(models.Model):
    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=250, verbose_name="Имя")
    phone = models.CharField(max_length=16, verbose_name="Номер телефона")
    email = models.EmailField(max_length=64, null=True, blank=True, verbose_name="Email адрес")
    address = models.TextField(max_length=250, verbose_name="Адрес доставки")

    @classmethod
    def create(cls, name, phone, email, address):
        customer = cls(name=name, phone=phone, email=email, address=address)
        return customer

    def __unicode__(self):
        return "%s - %s" % (self.name, self.phone)


class Order(models.Model):
    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customer, on_delete=models.SET('Удалено'))
    product = models.ForeignKey(GenericProduct, on_delete=models.SET('Удалено'))
    status = models.CharField(max_length=16, default='new', choices=STATUS_CHOICES, verbose_name="Статус")
    comments = models.TextField(max_length=250, null=True, blank=True, verbose_name="Комментарий клиента")
    staff_comments = models.TextField(max_length=250, null=True, blank=True, verbose_name="Служебный комментарий")
    created_date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Дата создания")
    updated_date = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name="Дата обновления")
    updated_by = models.CharField(max_length=32, blank=True, verbose_name="Кем обновлено")

    @classmethod
    def create(cls, product, customer, comments):
        order = cls(product=product, customer=customer, comments=comments, status='new', updated_by='admin')
        return order

    def __unicode__(self):
        return "%03d" % self.id
