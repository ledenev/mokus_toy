# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from django.db.models import Q
from django.views.generic import TemplateView
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from .models import Order, Customer
from .forms import OrderForm
from garden_app.models import GenericProduct
from garden_proj import settings
from telepot import Bot


class OrderIndexView(TemplateView):
    template_name = "order.html"
    context = dict()

    def get(self, request, *args, **kwargs):
        content = get_object_or_404(Order, slug=kwargs['slug'])
        self.context['content'] = content
        return render(request, self.template_name, self.context)


class OrderProductView(TemplateView):
    template_name = "order.html"
    context = dict()

    def get(self, request, *args, **kwargs):
        product = get_object_or_404(GenericProduct, Q(is_published=True), slug=kwargs['product'])
        self.context['product'] = product
        self.context['form'] = OrderForm()
        return render(request, self.template_name, self.context)

    def post(self, request, *args, **kwargs):
        form = OrderForm(request.POST)
        product = get_object_or_404(GenericProduct, Q(is_published=True), slug=kwargs['product'])
        if form.is_valid():
            new_customer = Customer.create(form.cleaned_data.get('name'), form.cleaned_data.get('phone'),
                                           form.cleaned_data.get('email'), form.cleaned_data.get('address'))
            new_customer.save()
            new_order = Order.create(product, new_customer, form.cleaned_data.get('comments'), )
            new_order.save()

            msg_subject = 'Новый заказ #%d на сайте mokus.by' % new_order.id
            from_email = settings.EMAIL_HOST_USER
            to_email = settings.EMAIL_TO
            msg_params = {'product': product,
                          'customer': new_customer,
                          'comments': form.cleaned_data.get('comments'),
                          'order_id': new_order.id}
            msg_plain = render_to_string('mail/new_order.txt', msg_params)

            send_mail(msg_subject, msg_plain, from_email, [to_email], fail_silently=False)

            # send Telegram notification
            bot = Bot(settings.TELEGRAM_BOT_ID)
            bot.sendMessage(settings.TELEGRAM_GROUP_ID, msg_plain)
            bot.sendMessage(settings.TELEGRAM_GROUP_ID, new_customer.phone)

            self.template_name = 'order_ok.html'
        else:
            self.context['product'] = product
            self.context['form'] = form
            # self.template_name = 'order_error.html'
        return render(request, self.template_name, self.context)
