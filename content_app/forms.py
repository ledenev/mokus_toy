from django import forms
from redactor.widgets import RedactorEditor
from .models import ContentPage


class ContentPageAdminForm(forms.ModelForm):
    class Meta:
        model = ContentPage
        fields = '__all__'
        widgets = {
           'content': RedactorEditor(),
        }
