from django.contrib import admin
from .forms import ContentPageAdminForm
from .models import ContentPage


class ContentPageAdmin(admin.ModelAdmin):
    form = ContentPageAdminForm
    list_display = ["title", "slug"]
    search_fields = "title", "description"
    ordering = ['title']

admin.site.register(ContentPage, ContentPageAdmin)
