# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.shortcuts import render, get_object_or_404
from .models import ContentPage


class ContentView(TemplateView):
    template_name = "content.html"
    context = dict()

    def get(self, request, *args, **kwargs):
        content = get_object_or_404(ContentPage, slug=kwargs['slug'])
        self.context['content'] = content
        return render(request, self.template_name, self.context)
