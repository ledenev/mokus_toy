# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db import models


class ContentPage(models.Model):
    class Meta:
        verbose_name = "Статическая страница"
        verbose_name_plural = "Статические страницы"
    title = models.CharField(max_length=250, default="null", verbose_name="Заголовок")
    slug = models.SlugField(max_length=250, unique=True, verbose_name="Web ссылка")
    content = models.TextField(max_length=32000, verbose_name="Содержание")

    def get_absolute_url(self):
        return reverse("content", kwargs={"slug": self.slug})

    def __unicode__(self):
        return self.title
