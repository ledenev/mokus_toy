from django.conf.urls import url

from .views import ContentView

urlpatterns = [
    url(r'^(?P<slug>[\w-]+)/$', ContentView.as_view(), name='content'),
]
