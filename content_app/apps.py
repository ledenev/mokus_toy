# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig


class ContentAppConfig(AppConfig):
    name = "content_app"
    verbose_name = "Статические данные"


# TODO: S3 Add SEO staff for all static pages
