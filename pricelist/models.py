# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.html import format_html


class PriceList(models.Model):
    class Meta:
        verbose_name = "Прайс листы"
        verbose_name_plural = "Прайс листы"
    file = models.FileField()
    status = models.CharField(max_length=32, default='Новый', verbose_name='Статус')
    comment = models.CharField(max_length=1000, null=True, blank=True, verbose_name='Коментарий')
    diff_status = models.CharField(max_length=1000, null=True, blank=True, verbose_name='Информация')
    upload_date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Дата загрузки")
    updated_date = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name="Дата обновления")
    log_file = models.FileField(null=True, blank=True)

    def diff_status_show(self):
        return format_html(self.diff_status)
    diff_status_show.short_description = "Отличия"
