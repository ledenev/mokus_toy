# -*- coding: utf-8 -*-
from django.contrib import admin
from .forms import *
from .models import *
from Price import Price
from django.contrib import messages
import uuid


class PriceListPageAdmin(admin.ModelAdmin):
    form = PriceListAdminForm
    list_display = ['upload_date', 'status', 'diff_status_show', 'comment']
    readonly_fields = ('status', 'upload_date', 'updated_date', 'diff_status_show', 'log_file')
    actions = ['process_price_list']

    fieldsets = (
        ('Основные парамерты', {
            'fields': ('file', 'comment')
        }),
        ('Статус', {
            'fields': ('status', 'diff_status_show', 'upload_date', 'updated_date'),
        }),
        ('Логи', {
            'fields': ('log_file',)
        }),
    )

    def process_price_list(self, request, queryset):
        for product in queryset:
            if product.status == u'Новый':
                price = Price(product.file.name, product.log_file.name)
                update_result = u'%d продуктов было обновлено' % price.price_process()
                product.diff_status += u'<br>\n<b>%s</b>' % update_result
                product.status = u'Обработан'
                product.save()
                self.message_user(request, update_result)
            else:
                self.message_user(request, 'Прайс от ' + product.upload_date.strftime(
                    '%Y-%m-%d %H:%M:%S') + ' уже был обработан', level=messages.WARNING)

    process_price_list.short_description = "Обработать прайслист"

    def save_model(self, request, obj, form, change):
        if obj.diff_status is None:
            obj.save()
            obj.log_file = './logs/%s.log' % uuid.uuid4()
            price = Price(obj.file.name, obj.log_file.name)
            obj.diff_status = price.calc_price_diff()
        obj.save()


    def get_readonly_fields(self, request, obj=None):
        if obj:  # editing an existing object
            return self.readonly_fields + ('file',)
        return self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(PriceList, PriceListPageAdmin)
