# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig


class PriceListAppConfig(AppConfig):
    name = "pricelist"
    verbose_name = "Прайсы"
