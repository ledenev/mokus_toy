# -*- coding: utf-8 -*-
import codecs
import datetime
from garden_app.generic_model import GenericProduct
from garden_proj.settings import MEDIA_ROOT
import xml.etree.ElementTree as ET
import re


class Price:
    def __init__(self, filename, logfile):
        self.price_list_tree = ET.parse('/'.join([MEDIA_ROOT, filename]))
        self.logfile = codecs.open('/'.join([MEDIA_ROOT, logfile]), 'a', 'utf-8')
        self.write_log(u'Прайслист %s был успешно загружен' % filename)
        self.price_data = []
        # TODO: Переписать на какой-нибудь упорядоченный спикок
        self.diff_status = dict(product_new=0, product_same=0, price_same=0, price_lower=0, price_higher=0, pub_same=0,
                                pub_add=0, pub_del=0, disc_same=0, disc_add=0, disc_del=0, prev_price_same=0,
                                prev_price_lower=0, prev_price_higher=0)

        self.read_price_file()

    def __del__(self):
        if not self.logfile.closed:
            self.logfile.close()

    def write_log(self, text):
        self.logfile.write(u'%s - %s\r\n' % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), text))

    def get_diff_status_string(self):
        status_string = ''
        for key in self.diff_status:
            status_string += "%s - %s <br>\n" % (key, self.diff_status[key])

        d = {'product_new': 'Продукты - Новые (пропущены)',
             'product_same': 'Продукты - Существующие',
             'price_same': 'Цена - Без изменений',
             'price_lower': 'Цена - Уменьшена',
             'price_higher': 'Цена - Увеличена',
             'pub_same': 'Публикация - Без изменений',
             'pub_add': 'Публикация - Добавлено',
             'pub_del': 'Публикация - Скрыто',
             'disc_same': 'Наличие скидки - Без изменений',
             'disc_add': 'Наличие скидки - Добавлено',
             'disc_del': 'Наличие скидки - Скрыто',
             'prev_price_same': 'Цена без скидки - Без изменений',
             'prev_price_lower': 'Цена без скидки - Уменьшена',
             'prev_price_higher': 'Цена без скидки - Увеличена',
             }
        pattern = re.compile(r'\b(' + '|'.join(d.keys()) + r')\b')
        return pattern.sub(lambda x: d[x.group()], status_string)

    @staticmethod
    def get_product(ext_id):
        return GenericProduct.objects.get(onliner_product_id=ext_id)

    def read_price_file(self):
        price_list_node = self.price_list_tree.getroot()
        if price_list_node.tag == 'pricelist':
            for product_node in price_list_node:
                product = {}
                for sub_node in product_node:
                    product[sub_node.tag] = sub_node.text
                self.price_data.append(product)
            #print(self.price_data)

    def calc_price_diff(self):
        for price_product in self.price_data:
            try:
                database_product = self.get_product(price_product['ext_id'])
                self.diff_status['product_same'] += 1

                # compare price
                if database_product.price == float(price_product['price']):
                    self.diff_status['price_same'] += 1
                elif database_product.price > float(price_product['price']):
                    self.diff_status['price_lower'] += 1
                elif database_product.price < float(price_product['price']):
                    self.diff_status['price_higher'] += 1

                # compare publication
                price_product_published = True if price_product['is_published'] == u'В наличии' else False
                if database_product.is_published == price_product_published:
                    self.diff_status['pub_same'] += 1
                elif database_product.is_published is True and price_product_published is False:
                    self.diff_status['pub_del'] += 1
                elif database_product.is_published is False and price_product_published is True:
                    self.diff_status['pub_add'] += 1

                # compare discounts
                price_is_discount = True if price_product['is_discount'] == u'Да' else False
                if database_product.is_published == price_is_discount:
                    self.diff_status['disc_same'] += 1
                elif database_product.is_published is True and price_is_discount is False:
                    self.diff_status['disc_del'] += 1
                elif database_product.is_published is False and price_is_discount is True:
                    self.diff_status['disc_add'] += 1

                # compare prev_price
                if database_product.prev_price == float(price_product['prev_price']):
                    self.diff_status['prev_price_same'] += 1
                elif database_product.prev_price > float(price_product['prev_price']):
                    self.diff_status['prev_price_lower'] += 1
                elif database_product.prev_price < float(price_product['prev_price']):
                    self.diff_status['prev_price_higher'] += 1

            except GenericProduct.DoesNotExist:
                self.diff_status['product_new'] += 1

        return self.get_diff_status_string()

    def price_process(self):
        products_updated = 0
        for price_product in self.price_data:
            is_changed = False
            try:
                database_product = self.get_product(price_product['ext_id'])
                self.diff_status['product_same'] += 1

                # compare price
                if database_product.price != int(price_product['price']):
                    is_changed = True
                    self.write_log(u'Цена продукта "%s" (%d) была изменена с %d на %s' %
                                   (database_product.name,
                                    database_product.id,
                                    database_product.price,
                                    price_product['price'],))
                    database_product.price = int(price_product['price'])

                # compare publication
                price_product_published = True if price_product['is_published'] == u'В наличии' else False
                if database_product.is_published != price_product_published:
                    is_changed = True
                    self.write_log(u'Видимость продукта "%s" (%d) была изменена с %d на %d' %
                                   (database_product.name,
                                    database_product.id,
                                    database_product.is_published,
                                    price_product_published,))
                    database_product.is_published = price_product_published

                # compare is_discount
                price_is_discount = True if price_product['is_discount'] == u'Да' else False
                if database_product.is_discount != price_is_discount:
                    is_changed = True
                    self.write_log(u'Наличие скидки у продукта "%s" (%d) была изменено с %d на %d' %
                                   (database_product.name,
                                    database_product.id,
                                    database_product.is_discount,
                                    price_is_discount,))
                    database_product.is_discount = price_is_discount

                # compare prev_price
                if database_product.prev_price != int(price_product['prev_price']):
                    is_changed = True
                    self.write_log(u'Цена продукта без скидки "%s" (%d) была изменена с %d на %s' %
                                   (database_product.name,
                                    database_product.id,
                                    (database_product.prev_price or 0),  # analog NVL
                                    price_product['prev_price'],))
                    database_product.prev_price = int(price_product['prev_price'])

                if is_changed:
                    database_product.save()
                    products_updated += 1

            except GenericProduct.DoesNotExist:
                self.write_log(u'Продукта "%s" (%s) нет в каталоге на сайте ' % (price_product['name'],
                                                                                 price_product['ext_id'],))

        return products_updated
