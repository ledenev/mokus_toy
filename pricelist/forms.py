from django import forms
from redactor.widgets import RedactorEditor
from .models import PriceList


class PriceListAdminForm(forms.ModelForm):
    class Meta:
        model = PriceList
        fields = '__all__'
        widgets = {
           'content': RedactorEditor(),
        }
